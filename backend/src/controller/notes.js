const express = require('express');
const PDFDocument = require('pdfkit');
const notesService = require('../services/notes');
const router = express.Router();

router.get('/notes', async (req, res, next) => {
  try {
    const notes = await notesService.findAll();
    res.status(200).json(notes);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Error when get all notes"
    });
    next(err);
  }
});

router.get('/notes/generate-pdf', async (req, res, next) => {
  try {
    const id = req.query.id || null;
    if (!id) throw { message: 'id cannot be empty' };
    const data = await notesService.findOne(id);
    if (!data) throw { message: 'empty note' };
    var myDoc = new PDFDocument({ bufferPages: true, font: 'Courier' });

    let buffers = [];
    myDoc.on('data', buffers.push.bind(buffers));
    myDoc.on('end', () => {
      let pdfData = Buffer.concat(buffers);
      res.writeHead(200, {
        'Content-Length': Buffer.byteLength(pdfData),
        'Content-Type': 'application/pdf',
        'Content-Disposition': 'attachment;filename=notes.pdf',
      }).end(pdfData);
    });
    myDoc.fontSize(20).text(data.title);
    myDoc.fontSize(12).text("\n" + data.content);
    myDoc.fontSize(12).text("\nTags : " + data.tags.map(t => t.tag).toString());
    myDoc.end();
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Error when generate PDF"
    });
    next(err);
  }
});

router.get('/notes/search', async (req, res, next) => {
  try {
    const textSearch = req.query.text || null;
    const tags = req.query.tag || null;
    const params = { text: textSearch, tags: tags };
    const notes = await notesService.findNotesBySearch(params);
    res.status(200).json(notes);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Error when get all notes"
    });
    next(err);
  }
});

router.get('/notes/:id', async (req, res, next) => {
  try {
    const id = req.params.id;
    const note = await notesService.findOne(id);
    res.status(200).json(note);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Error when get one note"
    });
    next(err);
  }
});

router.post('/notes', async (req, res, next) => {
  try {
    const params = req.body;
    const note = await notesService.create(params);
    res.status(200).json(note);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Error when create note"
    });
    next(err);
  }
});

router.put('/notes', async (req, res, next) => {
  try {
    const params = req.body;
    const note = await notesService.update(params);
    res.status(200).json(note);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Error when update note"
    });
    next(err);
  }
});

router.put('/notes/:id/pin', async (req, res, next) => {
  try {
    const params = req.params.id;
    const note = await notesService.updatePin(params);
    res.status(200).json(note);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Error when update note pin"
    });
    next(err);
  }
});

router.delete('/notes/:id', async (req, res, next) => {
  try {
    const params = req.params.id;
    const note = await notesService.remove(params);
    res.status(200).json(note);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Error when remove note"
    });
    next(err);
  }
});

module.exports = router;