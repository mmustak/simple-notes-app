const express = require('express');
const tagsService = require('../services/tags');
const router = express.Router();

router.get('/tags', async (req, res, next) => {
  try {
    const tags = await tagsService.findAll();
    res.status(200).json(tags);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Error when get all tags"
    });
    next(err);
  }
});

router.post('/tags', async (req, res, next) => {
  try {
    const params = req.body;
    const tags = await tagsService.create(params);
    res.status(200).json(tags);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Error when create tag"
    });
    next(err);
  }
});

router.delete('/tags/:id', async (req, res, next) => {
  try {
    const params = req.params.id;
    const tags = await tagsService.remove(params);
    res.status(200).json(tags);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Error when remove tag"
    });
    next(err);
  }
});

module.exports = router;