const express = require('express');
const cors = require('cors');
const notesRoute = require('./controller/notes');
const tagsRoute = require('./controller/tags');

const app = express();
const PORT = process.env.PORT || 5000;

const corsOptions = {
  origin: '*',
  methods: ['GET', 'POST', 'DELETE', 'UPDATE', 'PUT']
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(notesRoute);
app.use(tagsRoute);

const server = app.listen(PORT, () =>
  console.log(`🚀 Server ready at: http://localhost:${PORT}`)
);
