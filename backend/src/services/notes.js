const prisma = require('../config/prisma');

async function findAll() {
  const result = await prisma.note.findMany({
    include: {
      tags: true,
    },
    orderBy: [
      { pin: 'desc' },
      { createdAt: 'desc' || undefined }
    ],
  });
  return result;
}

async function findNotesBySearch(query) {
  const filterText = {
    OR: [
      {
        title: {
          contains: query.text,
          mode: 'insensitive'
        }
      },
      {
        content: {
          contains: query.text,
          mode: 'insensitive'
        }
      }
    ]
  };
  const filterTags = {
    tags: {
      some: {
        OR: query.tags?.map(t => ({ tag: t }))
      }
    }
  };
  const result = await prisma.note.findMany({
    where: {
      OR: query.text ? filterText : undefined,
      AND: query.tags ? filterTags : undefined
    },
    include: {
      tags: true,
    },
    orderBy: [
      { pin: 'desc' },
      { createdAt: 'desc' || undefined }
    ],
  });
  return result;
}

async function findOne(id) {
  const result = await prisma.note.findUnique({
    where: {
      id: id
    },
    include: {
      tags: true,
    },
  });
  return result;
}

async function create(params) {
  const { title, content, pin, tags } = params;
  const result = await prisma.note.create({
    data: {
      title: title,
      content: content,
      pin: pin || false,
      tags: {
        connectOrCreate: tags?.map(tag => ({
          where: { tag: tag },
          create: { tag: tag }
        }))
      }
    }
  });
  return result;
}

async function update(params) {
  const { id, title, content, pin, tags } = params;
  await prisma.note.update({
    where: {
      id: id
    },
    data: {
      tags: {
        set: []
      }
    }
  });
  const result = await prisma.note.update({
    where: {
      id: id
    },
    data: {
      title: title,
      content: content,
      pin: pin || false,
      tags: {
        connectOrCreate: tags?.map(tag => ({
          where: { tag: tag },
          create: { tag: tag }
        }))
      }
    },
    include: {
      tags: true
    }
  });
  return result;
}

async function updatePin(id) {
  const note = await findOne(id);
  const result = await prisma.note.update({
    where: {
      id: id
    },
    data: {
      pin: !note.pin || false,
    },
    include: {
      tags: true
    }
  });
  return result;
}

async function remove(id) {
  const result = await prisma.note.delete({
    where: {
      id: id
    },
  });
  return result;
}

module.exports.findAll = findAll;
module.exports.findNotesBySearch = findNotesBySearch;
module.exports.findOne = findOne;
module.exports.create = create;
module.exports.update = update;
module.exports.updatePin = updatePin;
module.exports.remove = remove;