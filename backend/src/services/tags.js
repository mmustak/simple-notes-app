const prisma = require('../config/prisma');

async function findAll() {
  const result = await prisma.tag.findMany({
    orderBy: { tag: 'desc' },
  });
  return result;
}

async function create(params) {
  const result = await prisma.tag.createMany({
    data: [params],
    skipDuplicates: true
  });
  return result;
}

async function remove(id) {
  const result = await prisma.tag.delete({
    where: {
      id: id
    },
  });
  return result;
}

module.exports.findAll = findAll;
module.exports.create = create;
module.exports.remove = remove;