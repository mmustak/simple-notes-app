# Notes App

Aplikasi untuk mengelola catatan yang sederhana untuk pemenuhan kebutuhan code challenge. Aplikasi ini dibangun dengan frontend menggunakan [ReactJS](https://reactjs.org/), backend menggunakan [ExpressJS](https://expressjs.com/), dan database menggunakan [PostgreSQL](https://www.postgresql.org/).

## Cara Menjalankan Aplikasi

Sebelum menjalankan aplikasi harus sudah terinstall berikut:
- Git versi terbaru (2.38.0). Cara install Git bisa dilihat [disini](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
- Node versi 14.20.1 keatas dan NPM versi 6.14.17 keatas. Cara install Node dan NPM bisa dilihat [disini](https://nodejs.dev/en/learn/how-to-install-nodejs/).

Clone this repo:

```
git clone https://gitlab.com/mmustak/simple-notes-app.git
cd simple-notes-app/
```

Agar aplikasi bisa berjalan dengan baik, ada beberapa langkah yang harus dilakukan antara lain :

### 1. Install Database PostgreSQL

Jika sudah install database PostgreSQL sebelumnya, silahkan lewati langkah ini dan lanjutkan ke langkah berikutnya.

Untuk install database Postgre SQL, dapat menggunakan Docker Container. Silahkan install Docker terlebih dulu [disini](https://docs.docker.com/engine/install/). Lalu install docker-compose [disini](https://docs.docker.com/compose/install/). Kemudian install Postgres SQL dengan langkah berikut:

```
docker-compose -f database/docker-compose.yml up -d
```

### 2. Menjalankan Backend

Backend dibangun menggunakan [Express](https://expressjs.com/), [Prisma Client](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client) dan [PDFKit](https://pdfkit.org/). 

Masuk pada folder backend dan install dependency yang dibutuhkan dengan perintah berikut:

```
cd backend/
npm install
```

Buat file `.env` yang berisi informasi database url seperti berikut:

`DATABASE_URL="postgresql://postgres:123456@localhost:5432/"`

Jalankan perintah berikut untuk create database dan create table `note` dan `tag` yang sudah didefinisikan pada `prisma/schema.prisma`.

```
npx prisma migrate dev --name init
```

Setelah database sudah siap, selanjutnya jalankan aplikasi backend menggunakan perintah berikut:

```
npm run dev
```
Backend berjalan pada `http://localhost:5000/`

### 3. Menjalankan Frontend

Frontend dibangun dengan menggunakan [React](https://reactjs.org/), [React Router](https://reactrouter.com/en/main), [Material UI](https://mui.com/), [Axios](https://axios-http.com/).

Masuk pada folder frontend dan install dependency yang dibutuhkan dengan perintah berikut:

```
cd frontend/
npm install
```

Buat file `.env.local` yang berisi informasi backend api url seperti berikut:

`REACT_APP_HOST_API=http://localhost:5000/`

Setelah itu jalankan frontend dengan menggunakan perintah berikut:

```
npm start
```

Frontend berjalan pada `http://localhost:3000/`

## Tampilan Aplikasi

<details><summary>Home</summary>

![](./Home.png)

</details>

<details><summary>Notes</summary>

![](./Notes.png)

</details>

<details><summary>Create</summary>

![](./Create.png)

</details>

<details><summary>Tags</summary>

![](./Tags.png)

</details>

## Dokumentasi API

Berikut adalah Dokumentasi REST API yang bisa diakses melalui `http://localhost:5000/`

### `GET`

- `/notes` : mengambil data semua catatan
- `/notes/generate-pdf?id={idNote}` : download pdf berdasarkan id catatan
- `/notes/search?text={searchString}&tag[]={tagString}` : mengambil data catatan berdasarkan (`title` atau `content`) dan `tags`.
- `/notes/:id` : mengambil data satu catatan berdasarkan id catatan
- `/tags` : mengambil data semua tag

### `POST`

- `/notes` : membuat data catatan
  - Body :
    - `title: String` (required): judul dari catatan
    - `content: String` (optional): isi dari catatan
    - `pin: Boolean` (required): penanda untuk selalu diatas
    - `tags: TagsCreateInput[]` (optional): tag dari catatan
- `/tags` : membuat data tag
  - Body :
    - `tag: String` (required): nama tag

### `PUT`

- `/notes` : mengubah data catatan
  - Body :
    - `id: String` (required): id catatan
    - `title: String` (required): judul dari catatan
    - `content: String` (optional): isi dari catatan
    - `pin: Boolean` (required): penanda untuk selalu diatas
    - `tags: TagsCreateInput[]` (optional): tag dari catatan
- `/notes/:id/pin` : mengubah pin catatan berdasarkan id catatan

### `DELETE`

- `/notes/:id` : menghapus catatan berdasarkan id catatan
- `/tags/:id` : menghapus tag berdasarkan id tag