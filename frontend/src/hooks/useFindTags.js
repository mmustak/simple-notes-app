import { useEffect, useState } from "react";
import { getTags } from "../api/TagsApi";

const useFindTags = () => {
  const [tags, setTags] = useState(null);
  const [refresh, setRefresh] = useState(true);

  useEffect(() => {
    if (refresh)
      getTags()
        .then((result) => {
          setTags(result.data);
          setRefresh(false);
        })
        .catch((error) => {
          console.log(error.message);
        });
  }, [refresh]);

  return {
    tags,
    setRefresh
  };
};

export default useFindTags;