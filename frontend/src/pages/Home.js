import React from "react";

const Home = (props) => {
  return (
    <div className="div-content">
      <h1>NOTES-APP</h1>
      <p>This is Notes App for code challenge</p>
    </div>
  );
};

export default Home;