import React, { useEffect, useState } from "react";
import { Autocomplete, Button, CircularProgress, Grid, TextField, Typography, } from "@mui/material";
import Note from "../components/Note";
import { deleteNote, exportToPdf, getNotesSearchBy, updateNotesPin } from "../api/NotesApi";
import useFindTags from "../hooks/useFindTags";
import { useNavigate } from "react-router-dom";
// import { renderToFile } from "@react-pdf/renderer";
// import ReactPDF from "@react-pdf/renderer";
// import PdfTemplate from "../components/PdfTemplate";

const Notes = (props) => {
  const navigate = useNavigate();

  const [search, setSearch] = useState({ text: null, tag: null });
  const [data, setData] = useState([]);
  const { tags } = useFindTags();
  const [isLoading, setIsLoading] = useState(true);

  const saveFile = (response) => {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    // link.setAttribute('download', response.headers['Content-Disposition'].split("filename=")[1]);
    link.setAttribute('download', 'notes.pdf');
    document.body.appendChild(link);
    link.click();
  };

  useEffect(() => {
    if (isLoading)
      getNotesSearchBy(search)
        .then((result) => {
          setIsLoading(false);
          setData(result.data);
        })
        .catch((error) => {
          setIsLoading(false);
          console.log(error.message);
        });
  }, [isLoading, search]);

  const handleExportToPdf = async (id) => {
    // ReactPDF.render(<PdfTemplate title={data.title || ""} content={data.content || ""} tags={data.tags || []} />, `${__dirname}/example.pdf`);
    // await renderToFile(<PdfTemplate title={data.title || ""} content={data.content || ""} tags={data.tags || []} />, `${__dirname}/my-doc.pdf`);
    // console.log(data);
    exportToPdf({ id: id })
      .then((response) => {
        saveFile(response);
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  const handleNotePin = (id) => {
    updateNotesPin(id)
      .then((result) => {
        setIsLoading(true);
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  const handleNoteEdit = (id) => {
    navigate('/update/' + id);
  };

  const handleNoteDelete = (id) => {
    deleteNote(id)
      .then((result) => {
        setIsLoading(true);
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  const handleSearch = () => {
    setIsLoading(true);
  };

  return (
    <div className="div-content">
      <Grid container direction="row" >
        <TextField id="text" placeholder="notes" style={{ margin: '8px 8px 8px 0px', }} value={search.text || ""} onChange={(e) => setSearch(prev => ({ ...prev, text: e.target.value || null }))} />
        <Autocomplete
          selectOnFocus
          multiple
          limitTags={2}
          id="tags"
          value={search.tag || []}
          onChange={(e, v) => setSearch(prev => ({ ...prev, tag: v.length > 0 ? v : null }))}
          options={tags ? tags.map(t => t.tag) : []}
          renderInput={(params) => (
            <TextField {...params} placeholder="tags" />
          )}
          style={{ margin: '8px 8px 8px 0px', minWidth: 235 }}
        />
        <Button variant="contained" color="inherit" style={{ margin: 'auto 0' }} onClick={handleSearch}>Search</Button>
      </Grid>
      <Typography variant="h5" gutterBottom sx={{ mt: '10px' }}>{"List Notes"}</Typography>
      {isLoading ? <CircularProgress /> : <Grid container alignItems="flex-start" justifyContent="flex-start" columns={{ xs: 1, sm: 4, md: 12 }} spacing={2} >
        {
          data.length > 0
            ? data.map(note => (
              <Grid item key={"div-" + note.id}>
                <Note key={note.id}
                  data={note}
                  onPin={() => handleNotePin(note.id)}
                  onEdit={() => handleNoteEdit(note.id)}
                  onDelete={() => handleNoteDelete(note.id)}
                  onExport={() => handleExportToPdf(note.id)}
                />
              </Grid>
            ))
            : <Grid item>
              <Typography>{"notes not found"}</Typography>
            </Grid>
        }
      </Grid>}
    </div>
  );
};

export default Notes;