import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { getOneNote, updateNote } from "../api/NotesApi";
import AddNote from "../components/AddNote";
import AlertSnackbar from "../components/AlertSnackbar";

const title = "Update Note";
const buttonText = "update";
const defaultAlert = { open: false, message: "", severity: "success" };

const Update = (props) => {
  const params = useParams();
  const navigate = useNavigate();

  const [note, setNote] = useState(null);
  const [alertDialog, setAlertDialog] = useState(defaultAlert);

  useEffect(() => {
    const formatData = (data) => {
      return { ...data, tags: data.tags.map(t => t.tag) };
    };

    getOneNote(params.id)
      .then((result) => {
        setNote(formatData(result.data));
      })
      .catch((error) => {
        console.log(error.message);
      });
  }, [params]);

  const save = () => {
    if (note?.title)
      updateNote(note)
        .then((result) => {
          console.log(result.data);
          navigate('/notes');
        })
        .catch((error) => {
          console.log(error.message);
        });
    else setAlertDialog({ open: true, message: "Title must not be empty", severity: "error" });
  };

  return (
    <div className="div-content">
      <AlertSnackbar id="alert-dialog"
        open={alertDialog.open}
        onClose={() => setAlertDialog(defaultAlert)}
        message={alertDialog.message}
        severity={alertDialog.severity}
      />
      <AddNote title={title} buttonText={buttonText} note={note} setNote={setNote} submit={save} />
    </div>
  );
};

export default Update;