import React, { useState } from "react";
import { Chip, Container, Grid, IconButton, InputAdornment, OutlinedInput, Tooltip, Typography } from "@mui/material";
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import useFindTags from "../hooks/useFindTags";
import { createTag, deleteTag } from "../api/TagsApi";

const Tags = (props) => {
  const { tags, setRefresh } = useFindTags();

  const [data, setData] = useState(null);

  const handleDelete = (id) => {
    deleteTag(id)
      .then((result) => {
        setRefresh(true);
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  const addTags = () => {
    createTag(data)
      .then((result) => {
        setData(null);
        setRefresh(true);
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  return (
    <div className="div-content">
      <Typography variant="h5" gutterBottom align="center">{"List Tags"}</Typography>
      <Container style={{ margin: '6px auto', justifySelf: 'flex-start', width: '75%' }}>
        {tags?.map((t, i) => (
          <Chip key={"t-" + i} label={t.tag} variant="outlined" onDelete={() => handleDelete(t.id)} sx={{ m: '3px' }} />
        ))}
      </Container>
      <Grid container alignContent={"center"}>
        <OutlinedInput
          id="tag"
          value={data?.tag || ""}
          onChange={(e) => setData({ tag: e.target.value })}
          onKeyDown={(e) => { if (e.key === 'Enter') addTags(); }}
          endAdornment={
            <InputAdornment position="end">
              <Tooltip title="add">
                <IconButton
                  onClick={addTags}
                  edge="end"
                >
                  <AddCircleOutlineIcon />
                </IconButton>
              </Tooltip>
            </InputAdornment>
          }
          style={{ width: '150px', textAlign: 'center', margin: '15px auto' }}
        />
      </Grid>
    </div>
  );
};

export default Tags;