import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { createNote } from "../api/NotesApi";
import AddNote from "../components/AddNote";
import AlertSnackbar from "../components/AlertSnackbar";

const title = "Add Note";
const buttonText = "create";
const defaultAlert = { open: false, message: "", severity: "success" };

const Create = (props) => {
  const navigate = useNavigate();

  const [note, setNote] = useState(null);
  const [alertDialog, setAlertDialog] = useState(defaultAlert);

  const save = () => {
    if (note?.title)
      createNote(note)
        .then((result) => {
          navigate('/notes');
        })
        .catch((error) => {
          console.log(error.message);
        });
    else setAlertDialog({ open: true, message: "Title must not be empty", severity: "error" });
  };

  return (
    <div className="div-content">
      <AlertSnackbar id="alert-dialog"
        open={alertDialog.open}
        onClose={() => setAlertDialog(defaultAlert)}
        message={alertDialog.message}
        severity={alertDialog.severity}
      />
      <AddNote title={title} buttonText={buttonText} note={note} setNote={setNote} submit={save} />
    </div>
  );
};

export default Create;