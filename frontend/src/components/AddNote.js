import React from "react";
import { Autocomplete, Button, createFilterOptions, Grid, Stack, Switch, TextField, Typography } from "@mui/material";
import useFindTags from "../hooks/useFindTags";

const filter = createFilterOptions();

const AddNote = (props) => {
  const { title, buttonText, note, setNote, submit } = props;
  const { tags } = useFindTags();

  const handleChange = (key, value) => {
    setNote(prev => ({ ...prev, [key]: typeof value === 'boolean' ? value : value.length > 0 ? value : null }));
  };

  return (
    <React.Fragment>
      <Typography variant="h5" gutterBottom align="center">{title || "Note"}</Typography>
      <Grid container direction="column" >
        <TextField id="title" required
          label="Title"
          style={{ margin: '6px auto', justifySelf: 'flex-start', width: '75%' }}
          value={note?.title || ""}
          onChange={(e) => handleChange("title", e.target.value)}
          error={note?.title ? false : true}
          helperText={note?.title ? "" : "this field is required"}
        />
        <TextField id="content" multiline
          minRows={5}
          label="Content"
          style={{ margin: '6px auto', justifySelf: 'start', width: '75%' }}
          value={note?.content || ""}
          onChange={(e) => handleChange("content", e.target.value)}
        />
        <Autocomplete
          multiple
          value={note?.tags || []}
          onChange={(e, v) => handleChange("tags", v)}
          filterOptions={(options, params) => {
            const filtered = filter(options, params);
            const { inputValue } = params;
            const isExisting = options.some((option) => inputValue === option);
            if (inputValue !== '' && !isExisting) {
              filtered.push(inputValue);
            }
            return filtered;
          }}
          selectOnFocus
          clearOnBlur
          id="tags"
          options={tags ? tags.map(t => t.tag) : []}
          getOptionLabel={(option) => option}
          freeSolo
          style={{ margin: '6px auto', justifySelf: 'start', width: '75%' }}
          renderInput={(params) => (
            <TextField {...params} label="Tags" />
          )}
        />
        <Stack direction="row" spacing={1} alignItems="center" style={{ margin: '6px auto', justifySelf: 'start', width: '75%' }} >
          <Typography>Unpin</Typography>
          <Switch checked={note?.pin || false} onChange={(e) => handleChange("pin", e.target.checked ? true : false)} />
          <Typography>Pin</Typography>
        </Stack>
        <Button variant="contained" style={{ margin: '10px auto' }} onClick={submit} >{buttonText || "create"}</Button>
      </Grid>
    </React.Fragment>

  );
};

export default AddNote;