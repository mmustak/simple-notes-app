import { Route, Routes } from "react-router-dom";
import Create from "../pages/Create";
import Home from "../pages/Home";
import Notes from "../pages/Notes";
import Tags from "../pages/Tags";
import Update from "../pages/Update";
import Page404 from "./Page404";

const ContentRouter = () => {

  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/home" element={<Home />} />
      <Route path="/notes" element={<Notes />} />
      <Route path="/create" element={<Create />} />
      <Route path="/update/:id" element={<Update />} />
      <Route path="/tags" element={<Tags />} />
      <Route path="*" element={<Page404 />} />
    </Routes>
  );
};

export default ContentRouter;