import React, { useState } from "react";
import { Card, CardActions, CardContent, Grid, IconButton, Tooltip, Typography } from "@mui/material";
import PinIcon from '@mui/icons-material/PushPin';
import EditIcon from '@mui/icons-material/Edit';
import RemoveIcon from '@mui/icons-material/DeleteForever';
import PictureAsPdfOutlinedIcon from '@mui/icons-material/PictureAsPdfOutlined';

const ReadMore = ({ children }) => {
  const text = children;
  const [isReadMore, setIsReadMore] = useState(true);
  const toggleReadMore = () => {
    setIsReadMore(!isReadMore);
  };
  return (
    <Typography variant="body2" color="text.secondary">
      {isReadMore ? text.slice(0, 50) : text}
      {text.length > 50 && <span onClick={toggleReadMore} className="read-or-hide">
        {isReadMore ? "...read more" : " show less"}
      </span>}
    </Typography>
  );
};

const Note = (props) => {
  const { data, onPin, onEdit, onDelete, onExport } = props;
  return (
    <Card sx={{ width: 270, backgroundColor: 'lightBlue' }}>
      <CardContent style={{ minHeight: 120 }}>
        <Grid container direction="row" >
          <Grid item xs>
            <Typography variant="h5" gutterBottom>
              {data.title || ""}
            </Typography>
          </Grid>
          {data.pin && <Grid item xs={1}>
            <PinIcon />
          </Grid>}
        </Grid>
        <ReadMore>
          {data.content}
        </ReadMore>
        <Typography variant="body1" style={{ marginTop: 15 }}>{"tags : " + data.tags.map(t => t.tag).toString()}</Typography>
      </CardContent>
      <CardActions disableSpacing >
        <Typography variant="body2" color="text.secondary" style={{ marginLeft: 8 }}>
          {new Date(data.createdAt).toLocaleDateString(['ban', 'id'])}
        </Typography>
        <Grid container justifyContent="flex-end">
          <Tooltip title="export to pdf">
            <IconButton onClick={onExport}>
              <PictureAsPdfOutlinedIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title="pin">
            <IconButton onClick={onPin}>
              <PinIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title="edit">
            <IconButton onClick={onEdit}>
              <EditIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title="delete">
            <IconButton onClick={onDelete}>
              <RemoveIcon />
            </IconButton>
          </Tooltip>
        </Grid>
      </CardActions>
    </Card>
  );
};

export default Note;