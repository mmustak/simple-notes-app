import { Snackbar, Alert } from '@mui/material';
import React from 'react';

const AlertSnackbar = (props) => {
  const { onClose, message: messageProp, open, severity, ...other } = props;

  return (
    <Snackbar
      open={open}
      autoHideDuration={5000}
      onClose={() => onClose()}
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      {...other}
    >
      <Alert onClose={() => onClose()} severity={severity} >
        {messageProp}
      </Alert>
    </Snackbar>
  );
};

export default AlertSnackbar;