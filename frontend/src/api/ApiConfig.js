import axios from "axios";

const apiBaseURL = process.env.REACT_APP_HOST_API;

const api = axios.create({
  baseURL: apiBaseURL,
  headers: {
    "Content-Type": "application/json",
  },
});

export default api;