import api from "./ApiConfig";

export const getNotes = async () => {
  return await api.get('/notes');
};

export const getOneNote = async (id) => {
  return await api.get('/notes/' + id);
};

export const getNotesSearchBy = async (params) => {
  return await api.get('/notes/search', {
    params: params
  });
};

export const updateNotesPin = async (id) => {
  return await api.put('/notes/' + id + "/pin");
};

export const createNote = async (note) => {
  return await api.post('/notes', note);
};

export const updateNote = async (note) => {
  return await api.put('/notes', note);
};

export const deleteNote = async (id) => {
  return await api.delete('/notes/' + id);
};

export const exportToPdf = async (params) => {
  return await api.get('/notes/generate-pdf', { params: params, responseType: 'blob' });
};
