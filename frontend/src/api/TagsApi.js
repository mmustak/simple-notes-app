import api from "./ApiConfig";

export const getTags = async () => {
  return await api.get('/tags');
};

export const createTag = async (data) => {
  return await api.post('/tags', data);
};

export const deleteTag = async (id) => {
  return await api.delete('/tags/' + id);
};
