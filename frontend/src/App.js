import './App.css';
import ContentRouter from './components/ContentRouter';
import Header from './components/Header';

function App() {
  return (
    <div className='container'>
      <Header />
      <main>
        <ContentRouter />
      </main>
    </div>
  );
}

export default App;
